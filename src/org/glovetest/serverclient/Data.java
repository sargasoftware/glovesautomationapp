package  org.glovetest.serverclient;
import javafx.beans.property.SimpleStringProperty;
public class Data {
	
	//private SimpleStringProperty id;
	private SimpleStringProperty temp;
	private SimpleStringProperty bat;
	private SimpleStringProperty glvPress;
	private SimpleStringProperty gaskPres;

	Data(String temp,String bat,String gasPressure,String glovePressure)
	 {
		 this.temp=new SimpleStringProperty(temp);
		 this.bat=new SimpleStringProperty( bat);
		 this.gaskPres=new SimpleStringProperty(gasPressure);
		 this.glvPress=new SimpleStringProperty(glovePressure);
	 }

	public String getTemp() {
		return temp.get();
	}
	public void setTemp(String temp) {
		this.temp.set(temp);
	}
	public String getBat() {
		return bat.get();
	}
	public void setBat(String bat) {
		this.bat.set(bat);
	}
	public String getGlvPress() {
		return glvPress.get();
	}
	public void setGlvPress(String glvPress) {
		this.glvPress.set(glvPress);
	}
	public String getGaskPres() {
		return gaskPres.get();
	}
	public void setGaskPres(String gaskPres) {
		this.gaskPres.set(gaskPres);
	}
	 

	}



