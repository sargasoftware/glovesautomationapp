package org.glovetest.serverclient;
import com.jfoenix.controls.JFXButton;
import application.*;
import com.jfoenix.controls.JFXTextField;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;

public class EchoServer {

	ServerSocket ss;ServerSocket welcomeSocket=null;
	static int x;
	static int y;
	static int count=0;
	MonitorController mm;
	static int initx=20;
	static int inity=105;
	static  int row;
	static int column;
	static int position=0;
	static int horGap=400;
	static int verGap=250;

	public EchoServer(MonitorController mycontroller) {
		this.mm=mycontroller;
		try {
	          welcomeSocket = new ServerSocket(5940);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		System.out.println("runnnnnnnnn");
		while(true) {
			Socket connectionSocket=null;
			try
			{
				connectionSocket = welcomeSocket.accept();
				row=position/3;
				column=position%3;
				x=initx+column*horGap;
				y= inity+row*verGap;
				System.out.println("A new client is connected : " + connectionSocket);

				// obtaining input and out streams
				BufferedReader dis = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));

				System.out.println("Assigning new thread "+position+ "  for this client "+position);
				Thread t = new ClientHandler(connectionSocket,dis,mm);
				t.start();

			}
			catch (Exception e){
			}
		}
	}
}




class ClientHandler extends Thread
{
	static List<ClientInfo> al=new ArrayList<ClientInfo>();
	static Map<Integer,Pane> m3=new HashMap<Integer,Pane>();
	static Map<Button,Integer> m4 =new HashMap<Button,Integer>();
	BufferedReader  inFromClient;
	Socket connectionSocket;
	MonitorController m1;	
	String[] a;
	String st;
	String confirmData="recveived";
	String t1="";
	String b1="";
	String g1="";
	String g2="";
	String id="";
	Button clickedBtn;
	int clickedPosition;
	double ht=150;
	double w=310;
	private boolean mStop = false;

	public ClientHandler(Socket connectionSocket,BufferedReader dis,MonitorController mm) {
		this.connectionSocket=connectionSocket;
		this.inFromClient=dis;
		this.m1=mm;
	}


	public void run() 
	{
		Pane p=new Pane();
		p.setPrefWidth(w);
		p.setPrefHeight(ht);
		
		p.getStyleClass().add("table");

		TableView CustomerTable0=new TableView();
		CustomerTable0.setPrefWidth(295);
		CustomerTable0.setPrefHeight(90);
		CustomerTable0.setLayoutX(10);
		CustomerTable0.setLayoutY(10);
		//CustomerTable0.getStyleClass().add("custom-align");
		TableColumn c01=new TableColumn("Temp");
		c01.setId("temp1");
		c01.setPrefWidth(72);
		c01.getStyleClass().add("custom-align");



		TableColumn c02=new TableColumn("Battery");
		c02.setId("bat1");
		c02.setPrefWidth(67.0);
		c02.setMinWidth(5);
		c02.getStyleClass().add("custom-align");


		TableColumn c03=new TableColumn("GasPres");
		c03.setId("gasPressure1");
		c03.setPrefWidth(81);
		
		c03.getStyleClass().add("custom-align");


		TableColumn c04=new TableColumn("GlvPres");
		c04.setId("glvpressure1");
		c04.setPrefWidth(81);
                c04.getStyleClass().add("custom-align");

		CustomerTable0.getColumns().addAll(c01,c02,c03,c04);

		/*Button b0= new Button();
		b0.setId("closeButton");
		b0.setText("Close");
		b0.setLayoutX(220.0);
		b0.setLayoutY(109.0);
                b0.setPrefHeight(30);
                b0.setPrefWidth(77);
		b0.getStyleClass().add("custom");
		b0.getStyleClass().add("panne");*/
	
		//b0.setDisable(true);

                JFXButton b0= new JFXButton();
                b0.setId("closeButton");
		b0.setText("Close");
		b0.setLayoutX(220.0);
		b0.setLayoutY(109.0);
                b0.setPrefHeight(30);
                b0.setPrefWidth(77);
		b0.getStyleClass().add("custom");
	
                  
                JFXTextField txt1=new JFXTextField();
                txt1.setId("textArea1");
                txt1.setPrefHeight(30);
                txt1.setPrefWidth(130);
		txt1.setLayoutX(53.0);
		txt1.setLayoutY(110.0);
                txt1.getStyleClass().add("custom1");
                txt1.getStyleClass().add("textDec");
                
		//TextField t0=new TextField();
		//t0.setId("textArea1");
		//t0.setLayoutX(59.0);
		//t0.setLayoutY(110.0);
		///t0.getStyleClass().add("panne");
		
		
		//t0.getStyleClass().add("welcome-text");

		//t0.getStyleClass().add("text");
		/*Label l0=new Label();
		l0.setId("label1");
		l0.setText("ID::");
		
		l0.setLayoutX(27.0);
		l0.setLayoutY(112.0);
		l0.getStyleClass().add("labelstyle");*/
		 
              /*  Label pictureLabel = AwesomeDude
                .createIconLabel(AwesomeIcons.ICON_KEY, 32);
		pictureLabel.setLayoutX(310);*/
                
              //  Label l = AppFont.createIcon(Icon.TEST, 40);
                
                Label pictureLabe2 = AwesomeDude
                .createIconLabel(AwesomeIcons.ICON_USER, 32);
		pictureLabe2.setLayoutX(26);
                pictureLabe2.setLayoutY(108);
                
                
              /*  Button refreshButton = AwesomeDude
            .createIconButton(AwesomeIcons.ICON_REFRESH, "Refresh");*/

		p.getChildren().addAll(CustomerTable0,b0,txt1,pictureLabe2);


		ClientInfo cl=new ClientInfo();
		cl.connectionSocket=connectionSocket;
		cl.x1=EchoServer.x;
		cl.y1=EchoServer.y;
		cl.ro=EchoServer.row;
		cl.col=EchoServer.column;
		cl.pane=p;
		cl.position=EchoServer.position;
		cl.mClientThread = this;
		cl.mButton=b0;

		al.add(cl);
		System.out.println(al);
		m4.put(b0,EchoServer.position);





		Platform.runLater(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				p.setLayoutX(EchoServer.x);
				p.setLayoutY(EchoServer.y);
				m1.initParams(p);
				EchoServer.position++;
			}

		});

		m3.put(EchoServer.position,p);


		b0.setOnAction(new EventHandler<ActionEvent>(){



			public void handle(ActionEvent we)
			{
				clickedBtn = (Button)we.getSource();
				clickedPosition = m4.get(clickedBtn);
				m1.closeAction(m3,clickedPosition);
				m3.remove(clickedBtn);
				m4.remove(clickedBtn);

				ClientInfo clickedClient = getClientInfoForPosition(clickedPosition);
				if(clickedClient != null)
				{
					try {
						clickedClient.connectionSocket.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					int clkPosTmp = clickedPosition;
					for(int idx = clickedPosition+1 ; idx < al.size() ; idx++)
					{
						ClientInfo nextClient = al.get(idx);

						nextClient.position = clkPosTmp;
						nextClient.ro = nextClient.position/3;
						nextClient.col = nextClient.position%3;

						nextClient.x1 = EchoServer.initx + EchoServer.horGap*nextClient.col;
						nextClient.y1 = EchoServer.inity + EchoServer.verGap*nextClient.ro;

						nextClient.pane.setLayoutX(nextClient.x1);
						nextClient.pane.setLayoutY(nextClient.y1);

						clkPosTmp++;
						m4.put(nextClient.mButton,nextClient.position);
						m3.put(nextClient.position,nextClient.pane);
					}



					al.remove(clickedPosition);
					clickedClient.mClientThread.stopThread();
					EchoServer.position--;
				}
			}
		});

		System.out.println(clickedBtn);


		while (!mStop) 
		{
			try {

				System.out.println("running thread ");

				while((st = inFromClient.readLine()) != null)
				{
					System.out.println(st);
					System.out.println("runnnnnn");
					a=st.split(":");
					for(int i=0;i<a.length;i++)
					{
						if(i==0)
						{
							this.id=a[i]; 
							System.out.println(id);
						}
						else if(i==1)
						{
							this.t1=a[i];
							System.out.println(t1);
						}
						else if(i==2){
							this.b1=a[i];
							System.out.println(b1);
						}
						else if(i==3)
						{
							this.g1=a[i];
							System.out.println(g1);
						}
						else 
						{
							this.g2=a[i];
							System.out.println(g2);
						}
					}

					Platform.runLater(new Runnable(){

						@Override
						public void run() {
							final ObservableList<Data> data = FXCollections.observableArrayList(
									new Data(t1, b1, g1,g2 ));



							c01.setCellValueFactory(
									new PropertyValueFactory<Data, String>("temp"));

							c02.setCellValueFactory(
									new PropertyValueFactory<Data, String>("bat"));

							c03.setCellValueFactory(
									new PropertyValueFactory<Data, String>("gaskPres"));

							c04.setCellValueFactory(
									new PropertyValueFactory<Data, String>("glvPress"));

							txt1.setText("     "+id);	
							CustomerTable0.setItems(data);
						}

					});
				}
				//  Thread.sleep(100);


			} catch (Exception e) {
				e.printStackTrace();
			}

		}	

	}

	private ClientInfo getClientInfoForPosition(int pos)
	{
		if(pos < al.size())
		{
			return al.get(pos);
		}
		return null;
	}

	public void stopThread()
	{
		mStop = true;
	}

}








