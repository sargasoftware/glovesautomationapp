package org.glovetest.serverclient;

import java.net.Socket;

import javafx.scene.control.Button;
import javafx.scene.layout.Pane;

public class ClientInfo {

	public Socket connectionSocket;
	public Object inFromClient;
	public Object c;
	public double x1;
	public double y1;
	public Pane pane;
	public int position;
	public int ro;
	public int col;
	
	public ClientHandler mClientThread;
	public Button mButton;
}
