package application;

import com.Sarga.Database.RetrieveLoginDetails;
import com.jfoenix.controls.JFXButton;

import java.awt.Frame;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class LoginController {

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private TextField userName;

	@FXML
	private TextField password;

	@FXML
	private JFXButton login;

	private static boolean success;

	private static String roleAssign1;

	private static  String nameLogin;



	@FXML
	void onClickLogin(ActionEvent event) throws IOException {
		nameLogin=userName.getText();
		String passwordLogin=password.getText();
		String msg="";
		boolean errorCondition=false;
		Frame frame=new Frame();
		if((userName.getText().equals("")) && (password.getText().equals("")))
		{
			JOptionPane.showMessageDialog(frame,"Enter fields are empty");
		}
		else
		{

			new RetrieveLoginDetails(nameLogin,passwordLogin);
			System.out.println("perfect");

			if(success==false)
			{
				JOptionPane.showMessageDialog(frame,"You have entered an invalid username or password");
			}
			else
			{

				if(success==true)
				{
					System.out.println("login success");
					AnchorPane anchorPane2 = FXMLLoader.load(getClass().getResource("MainMenuPage.fxml"));
					Scene scene2 = new Scene(anchorPane2);
					Stage app_stage2=(Stage)((Node) event.getSource()).getScene().getWindow();
					app_stage2.setScene(scene2);
					app_stage2.show();
				}


				/*if(success==true&&roleAssign1.equals("User"))
				{
					System.out.println("sucess="+success);
					System.out.println("xx");
					Parent root2=FXMLLoader.load(getClass().getResource("Welcome.fxml"));
					Scene scene = new Scene(root2);
					Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
					app_stage.setScene(scene);
					app_stage.show();
				}

				if(success==true&&roleAssign1.equals("Admin"))
				{
					System.out.println("yyyyy");
					Parent root2=FXMLLoader.load(getClass().getResource("AdminPage.fxml"));
					Scene scene = new Scene(root2);
					Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
					app_stage.setScene(scene);
					app_stage.show();
				}
			}*/
				//}










				/*AnchorPane anchorPane2 = FXMLLoader.load(getClass().getResource("MainMenuPage.fxml"));
    	Scene scene2 = new Scene(anchorPane2);
        Stage app_stage2=(Stage)((Node) event.getSource()).getScene().getWindow();
        app_stage2.setScene(scene2);
        app_stage2.show();*/
			}
		}

	}

	@FXML
	void initialize() {
		assert userName != null : "fx:id=\"userName\" was not injected: check your FXML file 'FXMLDoc2.fxml'.";
		assert password != null : "fx:id=\"password\" was not injected: check your FXML file 'FXMLDoc2.fxml'.";
		assert login != null : "fx:id=\"login\" was not injected: check your FXML file 'FXMLDoc2.fxml'.";

	}

	public static void successFailure(boolean authentificationSucessFlag, String roleAssign) {
		success=authentificationSucessFlag;
		roleAssign1=roleAssign;
		System.out.println("roleAssign1 in login="+roleAssign1);
		System.out.println("sucess="+success);

	}

	public static String successResult() {
		return  roleAssign1;
	}

	public static String getUserName() {

		return nameLogin;
	}
}
