/**
 * Sample Skeleton for 'ActivateAccount.fxml' Controller Class
 */

package application;

import java.awt.Frame;
import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import com.Sarga.Database.DeactivateAccount;
import com.Sarga.Database.LoginAcessUsers;
import com.Sarga.Database.RetrieveAccountId;
import com.Sarga.Database.RetrieveAccountIdToActivateAccount;
import com.jfoenix.controls.JFXButton;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class ActivateAccountController {

	@FXML // ResourceBundle that was given to the FXMLLoader
	private ResourceBundle resources;

	@FXML // URL location of the FXML file that was given to the FXMLLoader
	private URL location;

	@FXML // fx:id="cancel"
	private Button cancel; // Value injected by FXMLLoader

	@FXML // fx:id="ok"
	private Button ok; // Value injected by FXMLLoader

	@FXML // fx:id="userId"
	private TextField userId; // Value injected by FXMLLoader
	
	
	private boolean errorCondition;// Value injected by FXMLLoader

	private String name1;

	private String accountID1;

	private String email1;

	private String password1;

	private String confirmPassword1;

	private String phoneNo1;

	private String role1;


	@FXML
	void clickToCancel(ActionEvent event) {
		if(!userId.getText().equals(""))
		{
			userId.setText("");
		}


	}

	@FXML
	void clickToOk(ActionEvent event) {
		String accountId=userId.getText();
		Frame frame=new Frame();


		if((userId.getText().equals("")))
		{
			JOptionPane.showMessageDialog(frame,"Enter the Account ID");

		}
		else
		{
			new RetrieveAccountIdToActivateAccount(ActivateAccountController.this,accountId);
			if(errorCondition==false)
			{
				JOptionPane.showMessageDialog(frame,"Invalid AccountId");

			}
			else
			{
				new LoginAcessUsers(name1,accountID1,email1,password1,confirmPassword1,phoneNo1,role1);

			}
		}


	}

	 

	@FXML // This method is called by the FXMLLoader when initialization is complete
	void initialize() {
		assert cancel != null : "fx:id=\"cancel\" was not injected: check your FXML file 'ActivateAccount.fxml'.";
		assert ok != null : "fx:id=\"ok\" was not injected: check your FXML file 'ActivateAccount.fxml'.";
		assert userId != null : "fx:id=\"userId\" was not injected: check your FXML file 'ActivateAccount.fxml'.";

	}

	public void sendLoginUsers(String name, String accountID, String email, String password, String confirmPassword,
			String phoneNo, String role) {
		name1=name;
		accountID1=accountID;
		email1=email;
		password1=password;
		confirmPassword1=confirmPassword;
		phoneNo1=phoneNo;
		role1=role;
		errorCondition=true;


	}
}
