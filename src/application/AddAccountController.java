/**
 * Sample Skeleton for 'AddAccount.fxml' Controller Class
 */

package application;

import java.awt.Frame;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;



import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class AddAccountController {

	ObservableList<String> choice1=FXCollections.observableArrayList("Admin","Engineer","Supervisor","Worker");

	@FXML // ResourceBundle that was given to the FXMLLoader
	private ResourceBundle resources;

	@FXML // URL location of the FXML file that was given to the FXMLLoader
	private URL location;

	@FXML // fx:id="cancel"
	private Button cancel; // Value injected by FXMLLoader

	@FXML // fx:id="accountId"
	private TextField accountId; // Value injected by FXMLLoader

	@FXML // fx:id="name"
	private TextField name; // Value injected by FXMLLoader

	@FXML // fx:id="emailId"
	private TextField emailId; // Value injected by FXMLLoader

	@FXML // fx:id="ok"
	private Button ok; // Value injected by FXMLLoader

	@FXML // fx:id="phoneNo"
	private TextField phoneNo; // Value injected by FXMLLoader

	@FXML // fx:id="confirmpass"
	private PasswordField confirmpass; // Value injected by FXMLLoader

	@FXML // fx:id="pass"
	private PasswordField pass; // Value injected by FXMLLoader

	@FXML // fx:id="choice"
	private ChoiceBox<String> choice; // Value injected by FXMLLoader




	@FXML
	void clickToOk(ActionEvent event) {

		String name1=name.getText();
		String accountId1=accountId.getText();
		String emailId1=emailId.getText();
		String password1=pass.getText();
		String confirmPassword1=confirmpass.getText();
		String choice1=choice.getValue();
		String phoneNo1=phoneNo.getText();

		String msg="";
		Frame frame=new Frame();
		boolean erroCndition = false;

		if((name.getText().equals("")) && (accountId.getText().equals("")) && (emailId.getText().equals(""))&& (pass.getText().equals(""))&& (confirmpass.getText().equals(""))&& (choice1.equals(""))&& (phoneNo.getText().equals("")))
		{
			msg=msg+"\nEnter fields are empty";
			erroCndition = true;
		}
		else
		{
			if((name.getText().equals("")))
			{
				msg=msg+"\nName Field Should not be empty";
				erroCndition = true;
			}else
			{
				Pattern p=Pattern.compile("[a-zA-Z]+\\.?");
				Matcher m= p.matcher(name.getText().toString());
				boolean f=m.matches();
				if(f!=true)
				{
					msg=msg+"\nName Field should Contains only Letters and Spaces";
					erroCndition = true;
				}
			}

			if((accountId.getText().equals("")))
			{
				msg=msg+"\nAccount Id Should not be empty";
				erroCndition = true;
			}

			if((emailId.getText().equals("")))
			{
				msg=msg+"\nEmail Field Should not be empty";
				erroCndition = true;
			}
			else
			{

				Pattern p1=Pattern.compile("^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$");
				Matcher m1= p1.matcher(emailId.getText().toString());
				boolean f1=m1.matches();
				if(f1!=true)
				{
					msg=msg+"\n Email field allows Only numbers and Letters And Email format should be in \"****@****.***\"";
					erroCndition = true;
				}
			}

			if((pass.getText().equals("")))
			{
				msg=msg+"\nPassword Field Should not be empty";
				erroCndition = true;
			}
			else
			{

				Pattern p1=Pattern.compile("(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}");
				Matcher m1= p1.matcher(pass.getText().toString());
				boolean f1=m1.matches();
				if(f1!=true)
				{
					msg=msg+"\nPassword field allows minimum 8 Characters.Password Should contain atleat one UpperCase, One LowerCase, One Digit, One Special character. no white Spaces are allowed";
					erroCndition = true;
				}
			}

			if((confirmpass.getText().equals("")))
			{
				msg=msg+"\nConfirm Password Field Should not be empty";
				erroCndition = true;
			}
			else
			{

				if(!confirmpass.getText().equals(pass.getText()))
				{
					msg=msg+"\nBoth Password and Confirm Password does not match";
					erroCndition = true;
				}
			}

			if((choice.getValue().equals("")))
			{
				msg=msg+"\nRole Field Should not be empty";
				erroCndition = true;
			}



			if((phoneNo.getText().equals("")))
			{
				msg=msg+"\nPhone Number Field Should not be empty";
				erroCndition = true;
			}
			else
			{
				Pattern p2=Pattern.compile("\\d{3}\\d{7}");
				Matcher m2= p2.matcher(phoneNo.getText().toString());
				boolean f2=m2.matches();
				if(f2!=true)
				{
					msg=msg+"\nPhone Number allows Only Numbers and It shold be 10 Numbers";
					erroCndition = true;
				}
			}


		}


		if(erroCndition!=true)
		{


			//new AddAccountInformation(name1, accountId1, emailId1, password1,confirmPassword1,choice1,phoneNo1);
			//System.out.println("ok");
			//System.exit(0);
			Thread t = new Thread(){
				@Override
				public void run()
				{
					new AddAccountInformation(name1, accountId1, emailId1, password1,confirmPassword1,choice1,phoneNo1);
				}
			};
			t.start();


			/*	Parent root2=FXMLLoader.load(getClass().getResource("Login.fxml"));
			Scene scene = new Scene(root2);
			Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			app_stage.setScene(scene);
			app_stage.show();*/

		}
		else
		{

			JOptionPane.showMessageDialog(frame,msg);


		}



	}

	@FXML
	void clickToCancel(ActionEvent event) {
		if((!name.getText().equals(""))||(!accountId.getText().equals(""))||(!emailId.getText().equals(""))|| (!pass.getText().equals(""))|| (!confirmpass.getText().equals(""))||(!choice.getValue().equals(""))||(!phoneNo.getText().equals("")))
		{
			name.setText("");
			accountId.setText("");
			emailId.setText("");
			pass.setText("");
			confirmpass.setText("");
			phoneNo.setText("");
			choice.setValue("");

		}
	}



	@FXML // This method is called by the FXMLLoader when initialization is complete
	void initialize() {
		assert cancel != null : "fx:id=\"cancel\" was not injected: check your FXML file 'AddAccount.fxml'.";
		assert accountId != null : "fx:id=\"accountId\" was not injected: check your FXML file 'AddAccount.fxml'.";
		assert name != null : "fx:id=\"name\" was not injected: check your FXML file 'AddAccount.fxml'.";
		assert emailId != null : "fx:id=\"emailId\" was not injected: check your FXML file 'AddAccount.fxml'.";
		assert ok != null : "fx:id=\"ok\" was not injected: check your FXML file 'AddAccount.fxml'.";
		assert phoneNo != null : "fx:id=\"phoneNo\" was not injected: check your FXML file 'AddAccount.fxml'.";
		assert confirmpass != null : "fx:id=\"confirmpass\" was not injected: check your FXML file 'AddAccount.fxml'.";
		assert pass != null : "fx:id=\"pass\" was not injected: check your FXML file 'AddAccount.fxml'.";
		assert choice != null : "fx:id=\"choice\" was not injected: check your FXML file 'AddAccountProfile.fxml'.";
		choice.setValue("");
		choice.setItems(choice1);

	}
}
