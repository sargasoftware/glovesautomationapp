package application;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class MainController {

	private static String objectName;

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private int count1;

	@FXML
	private int count2;

	@FXML
	private int count3;

	@FXML
	private int count4;

	@FXML
	private int countAdd;

	@FXML
	private int countAct;

	@FXML
	private int countDea;

	@FXML
	private int countModify;


	@FXML
	private int countModifyFinal;




	@FXML
	private JFXButton monitor;

	@FXML
	private JFXButton report;

	@FXML
	private JFXButton setting;

	@FXML
	private JFXButton help;

	@FXML
	private JFXHamburger hamburger;

	@FXML
	private JFXDrawer drawer;

	@FXML
	private VBox vbox31;

	@FXML
	private VBox vbox32;

	@FXML
	private VBox vbox33;

	@FXML
	private Text userNameText;


	@FXML
	private JFXButton accountSetting;

	@FXML
	private JFXButton DeviceSetting;

	@FXML
	private JFXButton addAccount;

	@FXML
	private JFXButton activateAccount;

	@FXML
	private JFXButton deactivateAccount;

	@FXML
	private JFXButton modifyAccount;

	@FXML
	private StackPane anchorPaneMain;

	@FXML
	private JFXButton userRecord;

	@FXML
	private JFXButton deviceRecord;

	@FXML
	private StackPane newLoadedPane1;

	@FXML
	private StackPane newLoadedPane2;

	@FXML
	private StackPane newLoadedPane3;

	@FXML
	private StackPane newLoadedPane4;

	@FXML
	private StackPane newLoadedPaneAdd;

	@FXML
	private StackPane newLoadedPaneAct;

	@FXML
	private StackPane newLoadedPaneDact;

	@FXML
	public StackPane newLoadedPaneModify;

	@FXML
	public StackPane newLoadedPaneModifyFinal;

	@FXML
	private int countDeviceSetting;

	@FXML
	private StackPane newLoadedPaneDevice;

	@FXML
	private ArrayList st1;

	private String roleAssign;

	private String USER_NAME;


	@FXML
	void onMouseMovedOnStackPane(MouseEvent event) {
		vbox31.setVisible(false);
		vbox32.setVisible(false);
		vbox33.setVisible(false);
		drawer.setVisible(false);
		userNameText.setText(null);

	}

	@FXML
	void onClickHelp(ActionEvent event) throws IOException {
		count4++;
		anchorPaneMain.getChildren().remove(newLoadedPane2);
		anchorPaneMain.getChildren().remove(newLoadedPane3);
		anchorPaneMain.getChildren().remove(newLoadedPane4);
		anchorPaneMain.getChildren().remove(newLoadedPaneAdd);
		anchorPaneMain.getChildren().remove(newLoadedPaneAct);
		anchorPaneMain.getChildren().remove(newLoadedPaneDact);
		anchorPaneMain.getChildren().remove(newLoadedPaneModify);
		anchorPaneMain.getChildren().remove(newLoadedPaneDevice);
		//MonitorController control= new MonitorController();		
		if(count4<=1)
		{
			newLoadedPane1 =  FXMLLoader.load(getClass().getResource("HelpFXMLDoc.fxml"));
		}
		anchorPaneMain.getChildren().add(newLoadedPane1);

		anchorPaneMain.setVisible(true);

	}

	@FXML
	void onClickMonitor(ActionEvent event) throws IOException {
		count1++;


		anchorPaneMain.getChildren().remove(newLoadedPane1);
		anchorPaneMain.getChildren().remove(newLoadedPane3);
		anchorPaneMain.getChildren().remove(newLoadedPane4);
		anchorPaneMain.getChildren().remove(newLoadedPaneAdd);
		anchorPaneMain.getChildren().remove(newLoadedPaneAct);
		anchorPaneMain.getChildren().remove(newLoadedPaneDact);
		anchorPaneMain.getChildren().remove(newLoadedPaneModify);
		anchorPaneMain.getChildren().remove(newLoadedPaneDevice);
		if(count1<=1)
		{

			newLoadedPane2 =  FXMLLoader.load(getClass().getResource("MonitorFXMLDoc.fxml"));
		}
		anchorPaneMain.getChildren().add(newLoadedPane2);

		anchorPaneMain.setVisible(true);


	}

	@FXML
	void onClickReport(ActionEvent event) {
		vbox33.setVisible(true);

	}

	@FXML
	void onClickSetting(ActionEvent event) {
		roleAssign=LoginController.successResult();

		USER_NAME=LoginController.getUserName();
		if(roleAssign.equals("Admin")){
			vbox31.setVisible(true);
		}
	}

	@FXML
	void onClickActivateAccount(ActionEvent event) throws IOException {
		//countAct++;
		anchorPaneMain.getChildren().remove(newLoadedPane1);
		anchorPaneMain.getChildren().remove(newLoadedPane2);
		anchorPaneMain.getChildren().remove(newLoadedPane3);
		anchorPaneMain.getChildren().remove(newLoadedPane4);


		anchorPaneMain.getChildren().remove(newLoadedPaneAdd);
		anchorPaneMain.getChildren().remove(newLoadedPaneDact);
		anchorPaneMain.getChildren().remove(newLoadedPaneModify);
		anchorPaneMain.getChildren().remove(newLoadedPaneDevice);
		//MonitorController control= new MonitorController();		
		if(countAct<=1)
		{
			newLoadedPaneAct =  FXMLLoader.load(getClass().getResource("ActivateAccount.fxml"));
		}
		anchorPaneMain.getChildren().add(newLoadedPaneAct);

		anchorPaneMain.setVisible(true);
	}

	@FXML
	void onClickAddAccount(ActionEvent event) throws IOException {
		countAdd++;
		anchorPaneMain.getChildren().remove(newLoadedPane1);
		anchorPaneMain.getChildren().remove(newLoadedPane2);
		anchorPaneMain.getChildren().remove(newLoadedPane3);
		anchorPaneMain.getChildren().remove(newLoadedPane4);

		anchorPaneMain.getChildren().remove(newLoadedPaneAct);
		anchorPaneMain.getChildren().remove(newLoadedPaneDact);
		anchorPaneMain.getChildren().remove(newLoadedPaneModify);
		anchorPaneMain.getChildren().remove(newLoadedPaneDevice);
		//MonitorController control= new MonitorController();		
		if(countAdd<=1)
		{
			newLoadedPaneAdd = FXMLLoader.load(getClass().getResource("AddAccountProfile.fxml"));
		}
		anchorPaneMain.getChildren().add(newLoadedPaneAdd);

		anchorPaneMain.setVisible(true);

	}

	@FXML
	void onClickDeactivateAccount(ActionEvent event) throws IOException {
		countDea++;
		anchorPaneMain.getChildren().remove(newLoadedPane1);
		anchorPaneMain.getChildren().remove(newLoadedPane2);
		anchorPaneMain.getChildren().remove(newLoadedPane3);
		anchorPaneMain.getChildren().remove(newLoadedPane4);
		anchorPaneMain.getChildren().remove(newLoadedPaneAdd);
		anchorPaneMain.getChildren().remove(newLoadedPaneAct);
		anchorPaneMain.getChildren().remove(newLoadedPaneModify);
		anchorPaneMain.getChildren().remove(newLoadedPaneDevice);
		if(countDea<=1)
		{
			newLoadedPaneDact =  FXMLLoader.load(getClass().getResource("DeactivateAccount.fxml"));
		}
		anchorPaneMain.getChildren().add(newLoadedPaneDact);

		anchorPaneMain.setVisible(true);


	}

	@FXML
	void onClickDevice(ActionEvent event) throws IOException {

		countDeviceSetting++;
		anchorPaneMain.getChildren().remove(newLoadedPane1);
		anchorPaneMain.getChildren().remove(newLoadedPane2);
		anchorPaneMain.getChildren().remove(newLoadedPane3);
		anchorPaneMain.getChildren().remove(newLoadedPane4);
		anchorPaneMain.getChildren().remove(newLoadedPaneAdd);
		anchorPaneMain.getChildren().remove(newLoadedPaneDact);
		anchorPaneMain.getChildren().remove(newLoadedPaneAct);
		anchorPaneMain.getChildren().remove(newLoadedPaneModify);
		if(countDeviceSetting<=1)
		{
			newLoadedPaneDevice =  FXMLLoader.load(getClass().getResource("Device.fxml"));
		}
		anchorPaneMain.getChildren().add(newLoadedPaneDevice);

		anchorPaneMain.setVisible(true);

	}


	@FXML
	void onClickModifyAccount(ActionEvent event) throws IOException {
		countModify++;
		anchorPaneMain.getChildren().remove(newLoadedPane1);
		anchorPaneMain.getChildren().remove(newLoadedPane2);
		anchorPaneMain.getChildren().remove(newLoadedPane3);
		anchorPaneMain.getChildren().remove(newLoadedPane4);
		anchorPaneMain.getChildren().remove(newLoadedPaneAdd);
		anchorPaneMain.getChildren().remove(newLoadedPaneDact);
		anchorPaneMain.getChildren().remove(newLoadedPaneAct);
		anchorPaneMain.getChildren().remove(newLoadedPaneDevice);
		if(countModify<=1)
		{
			newLoadedPaneModify =  FXMLLoader.load(getClass().getResource("ModifyAccount.fxml"));
		}
		anchorPaneMain.getChildren().add(newLoadedPaneModify);

		anchorPaneMain.setVisible(true);

	}

	@FXML
	void onMouseEnterToAccount(MouseEvent event) {
		vbox32.setVisible(true);
	}

	@FXML
	void onClickDevideRecord(ActionEvent event) throws IOException {
		count3++;
		anchorPaneMain.getChildren().remove(newLoadedPane2);
		anchorPaneMain.getChildren().remove(newLoadedPane1);
		anchorPaneMain.getChildren().remove(newLoadedPane4);
		anchorPaneMain.getChildren().remove(newLoadedPaneAdd);
		anchorPaneMain.getChildren().remove(newLoadedPaneDact);
		anchorPaneMain.getChildren().remove(newLoadedPaneAct);
		anchorPaneMain.getChildren().remove(newLoadedPaneDevice);
		anchorPaneMain.getChildren().remove(newLoadedPaneModify);

		if(count3<=1)
		{

			newLoadedPane3 =  FXMLLoader.load(getClass().getResource("DataRecord.fxml"));
		}
		anchorPaneMain.getChildren().add(newLoadedPane3);

		anchorPaneMain.setVisible(true);


	}

	@FXML
	void onclickUserRecord(ActionEvent event) throws IOException {
		count2++;
		anchorPaneMain.getChildren().remove(newLoadedPane2);
		anchorPaneMain.getChildren().remove(newLoadedPane3);
		anchorPaneMain.getChildren().remove(newLoadedPane1);
		anchorPaneMain.getChildren().remove(newLoadedPaneAdd);
		anchorPaneMain.getChildren().remove(newLoadedPaneDact);
		anchorPaneMain.getChildren().remove(newLoadedPaneAct);
		anchorPaneMain.getChildren().remove(newLoadedPaneDevice);
		anchorPaneMain.getChildren().remove(newLoadedPaneModify);
		if(count2<=1)
		{
			newLoadedPane4 =  FXMLLoader.load(getClass().getResource("UserRecordFXMLDoc.fxml"));
		}
		anchorPaneMain.getChildren().add(newLoadedPane4);

		anchorPaneMain.setVisible(true);
	}




	@FXML
	void initialize() {

		try {
			drawer.setVisible(false);

			VBox  vbox=FXMLLoader.load(getClass().getResource("FXMLDoc4.fxml"));
			// vbox.getStyleClass().add("button");



			for(Node node: vbox.getChildren()){
				System.out.println(" inside vbox children");
				System.out.println("Node type is:"+node.getClass());
				if(node instanceof ButtonBar)
				{
					ObservableList<Node> buttons = ((ButtonBar)node).getButtons();

					for(Node btnNode : buttons)
					{
						btnNode.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
							System.out.println("Mouse is clicked");
							switch (node.getAccessibleText())
							{
							case "Button-One":
								userNameText.setText(USER_NAME);
								break;

							case "Button-Two":
								//userNameText.setText("manage Account");
								break;

							case "Button-Three":
								File f = new File(objectName+".txt");
								System.out.println(f);
								if(f.exists() && !f.isDirectory()) 
								{

									f.delete();
								}

								System.exit(0);
								break;


							}
						});
					}




					HamburgerBackArrowBasicTransition burgerTask2=new HamburgerBackArrowBasicTransition(hamburger);
					burgerTask2.setRate(-1);
					hamburger.addEventHandler(MouseEvent.MOUSE_PRESSED, (MouseEvent e) -> {

						burgerTask2.setRate(burgerTask2.getRate()*-1);
						burgerTask2.play();

						drawer.setVisible(true);
						drawer.setSidePane(vbox);
					});
					//drawer.setVisible(false);
				}

			}
		}

		catch (IOException ex) {
			Logger.getLogger(SignInPageController.class.getName()).log(Level.SEVERE, null, ex);
		}
		assert anchorPaneMain != null : "fx:id=\"anchorPaneMain\" was not injected: check your FXML file 'FXMLDoc3.fxml'.";
		assert monitor != null : "fx:id=\"monitor\" was not injected: check your FXML file 'FXMLDoc3.fxml'.";
		assert report != null : "fx:id=\"report\" was not injected: check your FXML file 'FXMLDoc3.fxml'.";
		assert setting != null : "fx:id=\"setting\" was not injected: check your FXML file 'FXMLDoc3.fxml'.";
		assert help != null : "fx:id=\"help\" was not injected: check your FXML file 'FXMLDoc3.fxml'.";
		assert hamburger != null : "fx:id=\"hamburger\" was not injected: check your FXML file 'FXMLDoc3.fxml'.";
		assert drawer != null : "fx:id=\"drawer\" was not injected: check your FXML file 'FXMLDoc3.fxml'.";
		assert accountSetting != null : "fx:id=\"accountSetting\" was not injected: check your FXML file 'FXMLDoc3.fxml'.";
		assert DeviceSetting != null : "fx:id=\"DeviceSetting\" was not injected: check your FXML file 'FXMLDoc3.fxml'.";
		assert addAccount != null : "fx:id=\"addAccount\" was not injected: check your FXML file 'FXMLDoc3.fxml'.";
		assert activateAccount != null : "fx:id=\"activateAccount\" was not injected: check your FXML file 'FXMLDoc3.fxml'.";
		assert deactivateAccount != null : "fx:id=\"deactivateAccount\" was not injected: check your FXML file 'FXMLDoc3.fxml'.";
		assert modifyAccount != null : "fx:id=\"modifyAccount\" was not injected: check your FXML file 'FXMLDoc3.fxml'.";
		assert vbox31 != null : "fx:id=\"vbox31\" was not injected: check your FXML file 'FXMLDoc3.fxml'.";
		assert vbox32 != null : "fx:id=\"vbox32\" was not injected: check your FXML file 'FXMLDoc3.fxml'.";
		assert userRecord != null : "fx:id=\"userRecord\" was not injected: check your FXML file 'FXMLDoc3.fxml'.";
		assert deviceRecord != null : "fx:id=\"deviceRecord\" was not injected: check your FXML file 'FXMLDoc3.fxml'.";
	}

	public static void sendsessionObjectUser(String accountIdd) {
		objectName=accountIdd;
		System.out.println("objectName="+objectName);

	}





}	

