/**
 * Sample Skeleton for 'ModifyAccount.fxml' Controller Class
 */

package application;

import java.awt.Frame;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import com.Sarga.Database.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class ModifyAccountController {

	@FXML // ResourceBundle that was given to the FXMLLoader
	private ResourceBundle resources;

	@FXML // URL location of the FXML file that was given to the FXMLLoader
	private URL location;

	@FXML // fx:id="cancel"
	private Button cancel; // Value injected by FXMLLoader

	@FXML // fx:id="modify"
	private TextField modify; // Value injected by FXMLLoader

	@FXML // fx:id="ok"
	private Button ok; // Value injected by FXMLLoader

	@FXML
	private StackPane stackPane1;

	@FXML
	private AnchorPane anchorPane1;


	private String modifyingId;

	private boolean errorCondition;

	private String name;

	private String accountIdd;

	private String email;

	private String passwordField;

	private String confirmPasswordField;

	private String phone;


	@FXML
	private StackPane newLoadedPane11;

	@FXML
	private String role1;

	@FXML
	void clickToCancelModifyAccount(ActionEvent event) {
		if(!modify.getText().equals(""))
		{
			modify.setText("");
		}


	}


	@FXML
	void clickToModifyAccount(ActionEvent event) throws IOException {
		String accountId=modify.getText();
		Frame frame=new Frame();


		if((modify.getText().equals("")))
		{
			JOptionPane.showMessageDialog(frame,"Enter the Account ID");

		}
		else
		{
			new RetrieveAccountIdToModify(ModifyAccountController.this,accountId);
			if(errorCondition==false)
			{
				JOptionPane.showMessageDialog(frame,"Invalid Account Id");

			}
			else
			{

				/*Parent root2=FXMLLoader.load(getClass().getResource("ModifyCompletely.fxml"));
				Scene scene = new Scene(root2);
				Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
				app_stage.setScene(scene);
				app_stage.show();*/

				new RetrieveUserAccountInformation(ModifyAccountController.this,modifyingId);



				//new RetrieveUserAccountInformation(ModifyAccountController.this,modifyingId);

				Parent root2=FXMLLoader.load(getClass().getResource("ModifyCompletely.fxml"));
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ModifyCompletely.fxml"));
				Parent root = (Parent)fxmlLoader.load();          
				ModifyAccountFinalController controller = (fxmlLoader).<ModifyAccountFinalController>getController();
				System.out.println("okdone");
				controller.sendProfileToModify(name,accountIdd,email,passwordField,confirmPasswordField,phone,modifyingId);
				anchorPane1.setVisible(false);
				//stackPane1.getChildren().remove("anchorPane1");
				stackPane1.getChildren().add(root);
				/*Scene scene = new Scene(root);
				Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
				app_stage.setScene(scene);
				app_stage.show();*/


				/* 	Parent root2=FXMLLoader.load(getClass().getResource("ModifyCompletely.fxml"));
					FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ModifyCompletely.fxml"));
					System.out.println("okdone");
				   StackPane stackPane12 = fxmlLoader.load();       
					 stackPane1.getChildren().remove(anchorPane1);
					// anchorPane1.setVisible(false);
	            	stackPane1.getChildren().add(stackPane12);
	            	//ModifyAccountFinalController.setTextFields(name,accountIdd,email,passwordField,confirmPasswordField,phone,modifyingId);

	            	ModifyAccountFinalController.sendProfileToModify(name,accountIdd,email,passwordField,confirmPasswordField,phone,modifyingId);
	            	stackPane1.getChildren().add(anchorPane1);*/


				/*	FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ModifyCompletely.fxml"));
	            	//fxmlLoader.setLocation(Main.class.getResource("ModifyCompletely.fxml"));
	            	ModifyAccountFinalController controller = (fxmlLoader).<ModifyAccountFinalController>getController();
					System.out.println("okdone");
					controller.sendProfileToModify(name,accountIdd,email,passwordField,confirmPasswordField,phone,modifyingId);

					newLoadedPane11 =  FXMLLoader.load(getClass().getResource("ModifyCompletely.fxml"));
					stackPane1.getChildren().add(newLoadedPane11);
                    anchorPane1.setVisible(false);
					stackPane1.setVisible(true);*/

			}
		}



	}

	@FXML // This method is called by the FXMLLoader when initialization is complete
	void initialize() {
		assert cancel != null : "fx:id=\"cancel\" was not injected: check your FXML file 'ModifyAccount.fxml'.";
		assert modify != null : "fx:id=\"modify\" was not injected: check your FXML file 'ModifyAccount.fxml'.";
		assert ok != null : "fx:id=\"ok\" was not injected: check your FXML file 'ModifyAccount.fxml'.";

	}


	public void sendAccountId(String accountId1) {
		System.out.println("modifying id="+accountId1);
		modifyingId=accountId1;

		errorCondition=true;


	}


	public void sendInformation(String name1, String accountId, String email1, String password, String confirmPassword,
			String phoneNo, String role) {
		name=name1;
		accountIdd=accountId;
		email=email1;
		passwordField=password;
		confirmPasswordField=confirmPassword;
		phone=phoneNo;
		role1=role;
		System.out.println("name="+name);
		System.out.println("accountIdd="+accountIdd);
		System.out.println("email="+email);
		System.out.println("passwordField="+passwordField);
		System.out.println("confirmPasswordField="+confirmPasswordField);
		System.out.println("phone="+phone);
		System.out.println("role1="+role1);


	}
}
