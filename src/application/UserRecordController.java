package application;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class UserRecordController {

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private TableView<?> tableView2;

	@FXML
	private TableColumn<?, ?> userIDCol;

	@FXML
	private TableColumn<?, ?> accountIDCol;

	@FXML
	private TableColumn<?, ?> emailIDCol;

	@FXML
	private TableColumn<?, ?> contactNoCol;

	@FXML
	void initialize() {
		assert tableView2 != null : "fx:id=\"tableView2\" was not injected: check your FXML file 'UserRecordFXMLDoc.fxml'.";
		assert userIDCol != null : "fx:id=\"userIDCol\" was not injected: check your FXML file 'UserRecordFXMLDoc.fxml'.";
		assert accountIDCol != null : "fx:id=\"accountIDCol\" was not injected: check your FXML file 'UserRecordFXMLDoc.fxml'.";
		assert emailIDCol != null : "fx:id=\"emailIDCol\" was not injected: check your FXML file 'UserRecordFXMLDoc.fxml'.";
		assert contactNoCol != null : "fx:id=\"contactNoCol\" was not injected: check your FXML file 'UserRecordFXMLDoc.fxml'.";

	}
}
