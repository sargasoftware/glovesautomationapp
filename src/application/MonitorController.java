package application;

import com.jfoenix.controls.JFXButton;

import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;

import org.glovetest.serverclient.EchoServer;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

public class MonitorController {

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private AnchorPane anchorPane51;

	@FXML
	private JFXButton startServer;

	@FXML
	private Label serverLabel;

	@FXML
	void  onclickServer(ActionEvent event) {
		Thread t = new Thread(){
			@Override
			public void run()
			{
				new EchoServer(MonitorController.this);
			}
		};
		t.start();

		startServer.setText("StopServer");
		// pane11.setVisible(true);
		serverLabel.setText("Server Started");
	}

	public void initParams(Pane p) {
		anchorPane51.getChildren().add(p);

	}


	public void closeAction(Map<Integer, Pane> m3, int c) {
		Pane p3=m3.get(c);

		anchorPane51.getChildren().remove(p3);

	}

	@FXML
	void initialize() {
		assert anchorPane51 != null : "fx:id=\"anchorPane51\" was not injected: check your FXML file 'MonitorFXMLDoc.fxml'.";
		assert startServer != null : "fx:id=\"startServer\" was not injected: check your FXML file 'MonitorFXMLDoc.fxml'.";
		assert serverLabel != null : "fx:id=\"serverLabel\" was not injected: check your FXML file 'MonitorFXMLDoc.fxml'.";

	}
}
