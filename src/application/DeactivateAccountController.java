/**
 * Sample Skeleton for 'DeactivateAccount.fxml' Controller Class
 */

package application;

import java.awt.Frame;
import java.net.URL;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import com.Sarga.Database.DeactivateAccount;
import com.Sarga.Database.RetrieveAccountId;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class DeactivateAccountController {

	@FXML // ResourceBundle that was given to the FXMLLoader
	private ResourceBundle resources;

	@FXML // URL location of the FXML file that was given to the FXMLLoader
	private URL location;

	@FXML // fx:id="cancel"
	private Button cancel; // Value injected by FXMLLoader

	@FXML // fx:id="ok"
	private Button ok; // Value injected by FXMLLoader

	@FXML // fx:id="deactivate"
	private TextField deactivate; // Value injected by FXMLLoader

	private boolean errorCondition=false;

	private String deactivateId;




	@FXML
	void clickToCancelDeactivateAccount(ActionEvent event) {
		if(!deactivate.getText().equals(""))
		{
			deactivate.setText("");
		}

	}

	@FXML
	void clickToDeactivateaccount(ActionEvent event) {
		String accountId=deactivate.getText();
		Frame frame=new Frame();


		if((deactivate.getText().equals("")))
		{
			JOptionPane.showMessageDialog(frame,"Enter the Account ID");

		}
		else
		{
			new RetrieveAccountId(DeactivateAccountController.this,accountId);
			if(errorCondition==false)
			{
				JOptionPane.showMessageDialog(frame,"Invalid AccountId");

			}
			else
			{
				new DeactivateAccount(deactivateId);

			}
		}


	}


	@FXML // This method is called by the FXMLLoader when initialization is complete
	void initialize() {
		assert cancel != null : "fx:id=\"cancel\" was not injected: check your FXML file 'DeactivateAccount.fxml'.";
		assert ok != null : "fx:id=\"ok\" was not injected: check your FXML file 'DeactivateAccount.fxml'.";
		assert deactivate != null : "fx:id=\"deactivate\" was not injected: check your FXML file 'DeactivateAccount.fxml'.";

	}

	public void sendAccountId(String accountId) {
		System.out.println(accountId);
		deactivateId=accountId;

		errorCondition=true;






	}
}
