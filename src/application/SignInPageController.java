package application;

import com.jfoenix.controls.JFXButton;

import java.awt.TextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class SignInPageController {

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private JFXButton signin;

	@FXML
	private Hyperlink link;


	@FXML 
	private  AnchorPane anchorPane1;


	@FXML
	void onLinkClick(ActionEvent event) {

	}

	@FXML
	void onSignIn(ActionEvent event) throws IOException {
		anchorPane1 = FXMLLoader.load(getClass().getResource("Login.fxml"));
		Scene scene2 = new Scene(anchorPane1);
		Stage app_stage2=(Stage)((Node) event.getSource()).getScene().getWindow();
		app_stage2.setScene(scene2);
		app_stage2.show();

	}

	@FXML
	void onClickLogin(ActionEvent event) {

	}


	@FXML
	void initialize() {
		assert signin != null : "fx:id=\"signin\" was not injected: check your FXML file 'FXMLDoc1.fxml'.";
		assert link != null : "fx:id=\"link\" was not injected: check your FXML file 'FXMLDoc1.fxml'.";


	}
}
