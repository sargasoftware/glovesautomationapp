package application;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;

public class DrawerVboxController {

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private ButtonBar ButtonBar1;

	@FXML
	private Button button1;

	@FXML
	private ButtonBar buttonBar2;

	@FXML
	private Button button2;

	@FXML
	private ButtonBar buttonBar3;

	@FXML
	private Button button3;

	@FXML
	void initialize() {
		assert ButtonBar1 != null : "fx:id=\"ButtonBar1\" was not injected: check your FXML file 'FXMLDoc4.fxml'.";
		assert button1 != null : "fx:id=\"button1\" was not injected: check your FXML file 'FXMLDoc4.fxml'.";
		assert buttonBar2 != null : "fx:id=\"buttonBar2\" was not injected: check your FXML file 'FXMLDoc4.fxml'.";
		assert button2 != null : "fx:id=\"button2\" was not injected: check your FXML file 'FXMLDoc4.fxml'.";
		assert buttonBar3 != null : "fx:id=\"buttonBar3\" was not injected: check your FXML file 'FXMLDoc4.fxml'.";
		assert button3 != null : "fx:id=\"button3\" was not injected: check your FXML file 'FXMLDoc4.fxml'.";

	}
}
