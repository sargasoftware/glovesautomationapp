package application;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class GenerateKey {
	static SecretKey secKey;
	public static void main(String[] args) throws Exception
	{

		secKey = getSecretEncryptionKey();
		ObjectOutputStream objOut = new ObjectOutputStream(new FileOutputStream(new File("secretkey.key")));
		objOut.writeObject(secKey);
		System.out.println(secKey);
	}


	public static SecretKey getSecretEncryptionKey() throws Exception {
		KeyGenerator generator = KeyGenerator.getInstance("AES");
		generator.init(128); // The AES key size in number of bits
		SecretKey secKey = generator.generateKey();
		return secKey;

	}


}
