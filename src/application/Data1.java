package  application;
import javafx.scene.control.CheckBox;
public class Data1 {

	private String accountId;
	private String userName;
	private CheckBox checkBoxx;
	private String role;

	Data1(String idd,String user,String role, CheckBox c)
	{
		this.accountId=new String(idd);
		this.userName=new String( user);
		this.role=new String(role);
		this.checkBoxx= c;

	}


	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public CheckBox getCheckBoxx() {
		return checkBoxx;
	}

	public void setCheckBoxx(CheckBox checkBoxx) {
		this.checkBoxx = checkBoxx;
	}

}



