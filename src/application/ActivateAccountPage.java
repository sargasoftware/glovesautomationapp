package application;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

public class ActivateAccountPage implements Initializable{

	@FXML // ResourceBundle that was given to the FXMLLoader
	private static ResourceBundle resources;

	@FXML // URL location of the FXML file that was given to the FXMLLoader
	private static URL location;

	@FXML // fx:id="anchorpane1"
	private AnchorPane anchorpane1; // Value injected by FXMLLoader

	@FXML // fx:id="tableview1"
	private  TableView<Data1> tableview1; // Value injected by FXMLLoader

	@FXML // fx:id="accountId"
	private TableColumn<Data1, String> accountId; // Value injected by FXMLLoader

	@FXML // fx:id="userName"
	private  TableColumn<Data1, String> userName; // Value injected by FXMLLoader

	@FXML // fx:id="role"
	private  TableColumn<Data1, String> role; // Value injected by FXMLLoader

	@FXML // fx:id="chechBoxListToActivate"
	private  TableColumn<Data1, CheckBox> chechBoxListToActivate; // Value injected by FXMLLoader
	
	@FXML
    private JFXButton activateButton;

	
	private boolean errorCondition;

	private String name1;

	private String accountID1;

	private String email1;

	private String password1;

	private String confirmPassword1;

	private String phoneNo1;

	private String role1;


	private  String accountIdd1;


	private  String   userName1;

	private  String role11;
	
	AddAccountsTotableDataBase ac;

	//	@FXML
	//	ObservableList<Data> list= FXCollections.observableArrayList();

	//@FXML
   // ObservableList  data = FXCollections.observableArrayList();

	
	@FXML
    ObservableList st;
	
	@FXML
    ObservableList st1;
	//private  int count;




	@Override
	public void initialize(URL location, ResourceBundle resources) {
	  ac=new AddAccountsTotableDataBase();
        try {
			 st=  ac.getData();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
          
					System.out.println(st);
		


		tableview1.setItems(st);



		accountId.setCellValueFactory(
				new PropertyValueFactory<Data1, String>("accountId"));

		userName.setCellValueFactory(
				new PropertyValueFactory<Data1, String>("userName"));

		role.setCellValueFactory(
				new PropertyValueFactory<Data1, String>("role"));

		chechBoxListToActivate.setCellValueFactory(
				new PropertyValueFactory<Data1, CheckBox>("checkBoxx"));
	}

		 @FXML
		    void onClickActivateAcoount(ActionEvent event) {
         st1= ac.getListToremove();
           
         st.removeAll(st1);  
          }
		 


	

	void initialize() {
		assert anchorpane1 != null : "fx:id=\"anchorpane1\" was not injected: check your FXML file 'ActivateAccount.fxml'.";
		assert tableview1 != null : "fx:id=\"tableview1\" was not injected: check your FXML file 'ActivateAccount.fxml'.";
		assert accountId != null : "fx:id=\"accountId\" was not injected: check your FXML file 'ActivateAccount.fxml'.";
		assert userName != null : "fx:id=\"userName\" was not injected: check your FXML file 'ActivateAccount.fxml'.";
		assert role != null : "fx:id=\"role\" was not injected: check your FXML file 'ActivateAccount.fxml'.";
		assert chechBoxListToActivate != null : "fx:id=\"chechBoxListToActivate\" was not injected: check your FXML file 'ActivateAccount.fxml'.";




		// TODO Auto-generated method stub

	}
	public void sendLoginUsers(String name, String accountID, String email, String password, String confirmPassword,
			String phoneNo, String role) {
		name1=name;
		accountID1=accountID;
		email1=email;
		password1=password;
		confirmPassword1=confirmPassword;
		phoneNo1=phoneNo;
		role1=role;
		errorCondition=true;


	}

	
		
	


}



