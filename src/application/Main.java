package application;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.ObjectInputStream;

import javax.crypto.SecretKey;

import com.Sarga.Database.DecryptLoginFile;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;


public class Main extends Application {
	private static String roleAssigned;
	private FileReader fr;
	private BufferedReader br;
	String sCurrentLine1;

	@Override
	public void start(Stage primaryStage) {
		try {

			File f = new File("e001.txt");
			ObjectInputStream objIn = new ObjectInputStream(new FileInputStream(new File("secretkey.key")));
			SecretKey secKeyFromFile = (SecretKey)objIn.readObject();



			System.out.println(f);
			if(f.exists()&& !f.isDirectory())
			{
				new DecryptLoginFile(f,secKeyFromFile);
				Parent root=FXMLLoader.load(getClass().getResource("MainMenuPage.fxml"));
				Scene scene = new Scene(root,400,400);
				scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				primaryStage.setScene(scene);
				primaryStage.show();
			}
			else
			{

				Parent root=FXMLLoader.load(getClass().getResource("Signin.fxml"));
				Scene scene = new Scene(root,400,400);
				scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				primaryStage.setScene(scene);
				primaryStage.show();
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

	public static void sendRoleassigned(String decryptedText2Role) {
		roleAssigned=decryptedText2Role;

	}
}
