package application;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;

public class DeviceSetting {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="stackPane1"
    private StackPane stackPane1; // Value injected by FXMLLoader

    @FXML // fx:id="anchorPane1"
    private AnchorPane anchorPane1; // Value injected by FXMLLoader

    @FXML // fx:id="modify"
    private TextField modify; // Value injected by FXMLLoader

    @FXML // fx:id="cancel"
    private Button cancel; // Value injected by FXMLLoader

    @FXML // fx:id="ok"
    private Button ok; // Value injected by FXMLLoader

    @FXML
    void clickToCancelModifyAccount(ActionEvent event) {

    }

    @FXML
    void clickToModifyAccount(ActionEvent event) {

    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert stackPane1 != null : "fx:id=\"stackPane1\" was not injected: check your FXML file 'Device.fxml'.";
        assert anchorPane1 != null : "fx:id=\"anchorPane1\" was not injected: check your FXML file 'Device.fxml'.";
        assert modify != null : "fx:id=\"modify\" was not injected: check your FXML file 'Device.fxml'.";
        assert cancel != null : "fx:id=\"cancel\" was not injected: check your FXML file 'Device.fxml'.";
        assert ok != null : "fx:id=\"ok\" was not injected: check your FXML file 'Device.fxml'.";

    }
}