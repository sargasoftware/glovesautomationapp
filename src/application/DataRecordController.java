package application;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class DataRecordController {

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private TableView<?> tableView1;

	@FXML
	private TableColumn<?, ?> dateCol;

	@FXML
	private TableColumn<?, ?> timeCol;

	@FXML
	private TableColumn<?, ?> gloveIDCol;

	@FXML
	private TableColumn<?, ?> testerIDCol;

	@FXML
	private TableColumn<?, ?> startPressureCol;

	@FXML
	private TableColumn<?, ?> endPressureCol;

	@FXML
	private TableColumn<?, ?> gasPressureCol;

	@FXML
	private TableColumn<?, ?> tempCol;

	@FXML
	private TableColumn<?, ?> resultCol;

	@FXML
	void initialize() {
		assert tableView1 != null : "fx:id=\"tableView1\" was not injected: check your FXML file 'DataRecord.fxml'.";
		assert dateCol != null : "fx:id=\"dateCol\" was not injected: check your FXML file 'DataRecord.fxml'.";
		assert timeCol != null : "fx:id=\"timeCol\" was not injected: check your FXML file 'DataRecord.fxml'.";
		assert gloveIDCol != null : "fx:id=\"gloveIDCol\" was not injected: check your FXML file 'DataRecord.fxml'.";
		assert testerIDCol != null : "fx:id=\"testerIDCol\" was not injected: check your FXML file 'DataRecord.fxml'.";
		assert startPressureCol != null : "fx:id=\"startPressureCol\" was not injected: check your FXML file 'DataRecord.fxml'.";
		assert endPressureCol != null : "fx:id=\"endPressureCol\" was not injected: check your FXML file 'DataRecord.fxml'.";
		assert gasPressureCol != null : "fx:id=\"gasPressureCol\" was not injected: check your FXML file 'DataRecord.fxml'.";
		assert tempCol != null : "fx:id=\"tempCol\" was not injected: check your FXML file 'DataRecord.fxml'.";
		assert resultCol != null : "fx:id=\"resultCol\" was not injected: check your FXML file 'DataRecord.fxml'.";

	}
}
