package com.Sarga.Security;


import java.io.IOException;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;


public class ZaCallbackHandler implements CallbackHandler {
	String name;
	String Password;

	public ZaCallbackHandler(String loginName, String loginPassword) {
		name=loginName;
		Password=loginPassword;

	}

	@Override
	public void handle(Callback[] callbackArray) throws IOException, UnsupportedCallbackException {
		NameCallback nameCallback=null;
		PasswordCallback passwordCallback=null;
		int counter=0;

		while(counter<callbackArray.length)
		{
			if(callbackArray[counter] instanceof NameCallback)
			{
				nameCallback=(NameCallback)callbackArray[counter++];
				System.out.println(nameCallback.getPrompt());
				//nameCallback.setName(new BufferedReader(new InputStreamReader(System.in)).readLine());
				nameCallback.setName(name);



			}else if(callbackArray[counter] instanceof PasswordCallback)
			{
				passwordCallback=(PasswordCallback)callbackArray[counter++];
				System.out.println(passwordCallback.getPrompt());
				//passwordCallback.setPassword(new BufferedReader(new InputStreamReader(System.in)).readLine().toCharArray());
				passwordCallback.setPassword(Password.toCharArray());


			}
		}


	}

}
