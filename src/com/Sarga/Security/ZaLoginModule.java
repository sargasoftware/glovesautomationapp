package com.Sarga.Security;

import java.awt.Frame;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;
import javax.swing.JOptionPane;

import com.Sarga.Database.EncryptLoginFile;

import application.LoginController;
import application.MainController;


public class ZaLoginModule implements LoginModule {

	public static String TEST_USERNAME;
	public static String TEST_PASSWORD;
	private CallbackHandler callbackHandler=null;
	private boolean authentificationSucessFlag=false;
	private Subject subject=null;
	private UserPrincipal userPrincipal;
	private RolePrincipal rolePrincipal;
	private static  String roleAssign;
	private static  String accountIdd;
	private static LoginController loginController1;
	private List<String> userGroups;
	private boolean commitSucceeded = false;

	String loginName;
	String loginPassword;


	/*public ZaLoginModule(String name, String password, String role, String accountId, LoginController loginController) {
		TEST_USERNAME=name;
		TEST_PASSWORD=password;
		roleAssign=role;
		accountIdd=accountId;
		loginController1=loginController;
		System.out.println("TEST_USERNAME="+TEST_USERNAME);
	
	}*/

	@Override
	public boolean abort() throws LoginException {
		System.out.println("abort");
		if (!authentificationSucessFlag) {
			return false;
		} else if (authentificationSucessFlag && !commitSucceeded) {
			authentificationSucessFlag = false;
			TEST_USERNAME = null;
			TEST_PASSWORD = null;
			userPrincipal = null;
		} else {
			logout();
		}
		return true;
	}

	@Override
	public boolean commit() throws LoginException {
		System.out.println("commit");
		if (!authentificationSucessFlag) {
			return false;
		}
		else
		{
			userPrincipal = new UserPrincipal(TEST_USERNAME);
			subject.getPrincipals().add(userPrincipal);

			if (userGroups != null && userGroups.size() > 0) {
				for (String groupName : userGroups) {
					rolePrincipal = new RolePrincipal(groupName);
					subject.getPrincipals().add(rolePrincipal);
				}

			}
			commitSucceeded = true;

			return true;
		}

	}

	@Override
	public void initialize(Subject subject, CallbackHandler callbackHandler, Map<String, ?> arg2, Map<String, ?> arg3) {
		this.subject=subject;
		this.callbackHandler=callbackHandler;
		System.out.println("initialize");

	}

	public boolean login() throws LoginException {

		Callback[] callbackArray=new Callback[2];
		callbackArray[0]=new NameCallback("Username:");
		callbackArray[1]=new PasswordCallback("Password:", false);

		try {
			callbackHandler.handle(callbackArray);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (UnsupportedCallbackException e) {
			e.printStackTrace();
		}

		String name=((NameCallback)callbackArray[0]).getName();
		String password=new String(((PasswordCallback)callbackArray[1]).getPassword());
		if(TEST_USERNAME.equals(name)&&TEST_PASSWORD.equals(password))
		{
			try {
				new EncryptLoginFile(TEST_USERNAME,TEST_PASSWORD,roleAssign,accountIdd);
				MainController.sendsessionObjectUser(accountIdd);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("Authentification is Success");

			// Assign the user roles
			if(roleAssign.equals("Admin"))
			{
				userGroups = this.getRoles();
				try {
					//AdminPageController.sendsessionObjectAdmin(employeeIdd);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			authentificationSucessFlag=true;
			System.out.println("login");
			
		}
		else
		{
			authentificationSucessFlag=false;
			
			throw new FailedLoginException("Authentification is Failure");

		}
       try {
    	   LoginController.successFailure(authentificationSucessFlag,roleAssign);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
        
		return authentificationSucessFlag;


	}


	@Override
	public boolean logout() throws LoginException {
		
		System.out.println("logout");
		authentificationSucessFlag = false;
		authentificationSucessFlag= commitSucceeded;
		subject.getPrincipals().clear();
		return true;

		//subject.getPrincipals().remove(userPrincipal);
		//subject.getPrincipals().remove(rolePrincipal);
		//return true;
	}





	public static   void send(String name, String password, String role, String accountId) {
		TEST_USERNAME=name;
		TEST_PASSWORD=password;
		roleAssign=role;
		accountIdd=accountId;
		//loginController1=loginController;
		System.out.println("TEST_USERNAME="+TEST_USERNAME);


	}
	private List<String> getRoles() {

		List<String> roleList = new ArrayList<>();
		roleList.add(TEST_USERNAME);
		System.out.println("role list="+roleList);
		return roleList;

	}








}
