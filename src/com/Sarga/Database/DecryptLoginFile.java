package com.Sarga.Database;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import application.LoginController;
import application.Main;
import application.MainController;

public class DecryptLoginFile {

	private SecretKeySpec symKey;
	private byte[] cipherText;
	private SecretKeySpec secKey;

	public DecryptLoginFile(File f, SecretKey fileSecretKey) throws Exception {

		BufferedReader br = null;
		FileReader fr = null;

		try {

			//br = new BufferedReader(new FileReader(FILENAME));
			fr = new FileReader(f);
			br = new BufferedReader(fr);

			String sCurrentLine;

			String[] arr=new String[3];
			int i=0;

			while ((sCurrentLine = br.readLine()) != null) {
				System.out.println("in decrypt="+sCurrentLine);
				arr[i]=sCurrentLine;
				i++;
				System.out.println(sCurrentLine);



			}
			//cipherText=arr[0].getBytes();
			byte[] b = DatatypeConverter.parseHexBinary(arr[0]);
			byte[] b1 = DatatypeConverter.parseHexBinary(arr[1]);
			byte[] b2 = DatatypeConverter.parseHexBinary(arr[2]);


			ObjectInputStream objIn = new ObjectInputStream(new FileInputStream(new File("secretkey.key")));
			SecretKey secKeyFromFile = (SecretKey)objIn.readObject();
			System.out.println("main key="+secKeyFromFile);

			String decryptedTextName = decryptText(b, secKeyFromFile);
			System.out.println("decryptedTextName="+decryptedTextName);

			String decryptedText1Password = decryptText1(b1, secKeyFromFile);
			System.out.println("decryptedTextPassword="+decryptedText1Password);


			String decryptedText2Role = decryptText2(b2, secKeyFromFile);
			System.out.println("decryptedText2Role="+decryptedText2Role);



			new RetrieveLoginDetails(decryptedTextName,decryptedText1Password);
			Main.sendRoleassigned(decryptedText2Role);


		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (br != null)
					br.close();

				if (fr != null)
					fr.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}



		}




	}

	private String decryptText2(byte[]b2, SecretKey secKeyFromFile) throws Exception {
		// AES defaults to AES/ECB/PKCS5Padding in Java 7
		Cipher aesCipher = Cipher.getInstance("AES");
		aesCipher.init(Cipher.DECRYPT_MODE, secKeyFromFile);
		byte[] bytePlainText = aesCipher.doFinal(b2);
		return new String(bytePlainText);
	}

	private String decryptText1(byte[] b1, SecretKey secKeyFromFile) throws Exception {
		// AES defaults to AES/ECB/PKCS5Padding in Java 7
		Cipher aesCipher = Cipher.getInstance("AES");
		aesCipher.init(Cipher.DECRYPT_MODE, secKeyFromFile);
		byte[] bytePlainText = aesCipher.doFinal(b1);
		return new String(bytePlainText);

	}

	private String decryptText(byte[] b, SecretKey fileSecretKey) throws Exception {
		// AES defaults to AES/ECB/PKCS5Padding in Java 7
		Cipher aesCipher = Cipher.getInstance("AES");
		aesCipher.init(Cipher.DECRYPT_MODE, fileSecretKey);
		byte[] bytePlainText = aesCipher.doFinal(b);
		return new String(bytePlainText);

	}

}
