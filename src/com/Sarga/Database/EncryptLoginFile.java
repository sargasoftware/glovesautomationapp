package com.Sarga.Database;



import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.xml.bind.DatatypeConverter;

public class EncryptLoginFile {

	public EncryptLoginFile(String tEST_USERNAME, String tEST_PASSWORD, String roleAssign, String accountIdd) throws Exception {

		System.out.println("role assign in"+roleAssign);

		Writer writer = null;

		try {
			File f=new File(accountIdd+".txt");
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(f), "utf-8"));
			//writer.write(tEST_USERNAME);



			ObjectInputStream objIn = new ObjectInputStream(new FileInputStream(new File("secretkey.key")));
			SecretKey secKeyFromFile = (SecretKey)objIn.readObject();

			System.out.println("obsecKey="+secKeyFromFile);
			byte[] cipherText = encryptText(tEST_USERNAME, secKeyFromFile);


			System.out.println("Original Text:" +tEST_USERNAME);
			System.out.println("AES Key (Hex Form):"+bytesToHex(secKeyFromFile.getEncoded()));
			System.out.println("Encrypted Text (Hex Form):"+bytesToHex(cipherText));


			writer.write(bytesToHex(cipherText));
			((BufferedWriter) writer).newLine();

			byte[] cipherText1 = encryptText1(tEST_PASSWORD, secKeyFromFile);
			writer.write(bytesToHex(cipherText1));
			((BufferedWriter) writer).newLine();


			byte[] cipherText2 = encryptText2(roleAssign, secKeyFromFile);
			writer.write(bytesToHex(cipherText2));
			((BufferedWriter) writer).newLine();

		} catch (IOException ex) {
			// Report
		} finally {
			try {writer.close();} catch (Exception ex) {/*ignore*/}
		}
	}





	private byte[] encryptText2(String roleAssign, SecretKey secKeyFromFile) throws Exception {
		// AES defaults to AES/ECB/PKCS5Padding in Java 7
		Cipher aesCipher = Cipher.getInstance("AES");
		aesCipher.init(Cipher.ENCRYPT_MODE, secKeyFromFile);
		byte[] byteCipherText = aesCipher.doFinal(roleAssign.getBytes());
		return byteCipherText;
	}





	/**
	 * Encrypts plainText in AES using the secret key
	 * @param plainText
	 * @param secKey
	 * @return
	 * @throws  
	 * @throws NoSuchAlgorithmException 
	 * @throws Exception
	 */


	public static byte[] encryptText(String tEST_USERNAME, SecretKey secKey) throws Exception {
		// AES defaults to AES/ECB/PKCS5Padding in Java 7
		Cipher aesCipher = Cipher.getInstance("AES");
		aesCipher.init(Cipher.ENCRYPT_MODE, secKey);
		byte[] byteCipherText = aesCipher.doFinal(tEST_USERNAME.getBytes());
		return byteCipherText;

	}

	public byte[] encryptText1(String tEST_PASSWORD, SecretKey secKey) throws Exception {
		// AES defaults to AES/ECB/PKCS5Padding in Java 7
		Cipher aesCipher = Cipher.getInstance("AES");
		aesCipher.init(Cipher.ENCRYPT_MODE, secKey);
		byte[] byteCipherText = aesCipher.doFinal(tEST_PASSWORD.getBytes());
		return byteCipherText;

	}


	/**
	 * gets the AES encryption key. In your actual programs, this should be safely
	 * stored.
	 * @return
	 * @throws Exception
	 */





	/**
	 * Convert a binary byte array into readable hex form
	 * @param hash
	 * @return
	 */
	private static String  bytesToHex(byte[] hash) {
		return DatatypeConverter.printHexBinary(hash);
	}


}
